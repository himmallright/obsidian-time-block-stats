Associations: [[Fall 2200]]
Tags: #daily_note
Status: #archived

---

# 2200-12-01

## Plans
- [x] 08:00 Clean Kitchen + Coffee #cleaning
- [x] 09:00 [[Obsidian Basics (Post)]] #website
        - [x] Debug Tests
        - [ ] First Draft Edit Session
- [x] 11:45 Workout #workout
- [ ] 14:20 Lunch #eating
- [ ] 12:20 Relax/Shower #relax
- [ ] 15:00 Done #idk

## Log/Notes
- Just a fake note to test with

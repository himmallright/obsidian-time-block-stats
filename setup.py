#!/usr/bin/env python
"""setuptools script for installing Obsidian Time Block Stats"""
import os
from setuptools import find_packages, setup

_project_root = os.path.abspath(os.path.dirname(__file__))

setup(
    name="otbs",
    version="0.0.1",
    author="Ryan Himmelwright",
    author_email="ryan.himmelwright@gmail.com",
    description="Parses an Obsidian note vault, grabs lines from daily planner notes, and calculates statistics.",
    license="GPL-3.0",
    keywords=["notes", "planning", "obsidian"],
    packages=find_packages(include=["otbs*"]),
    scripts=["bin/otbs"],
    install_requires=[],
)

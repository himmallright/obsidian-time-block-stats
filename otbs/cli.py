"""The CLI code for OTBS."""
import click
from otbs.parser import *
from otbs.stats import calc_category_hours
from typing import List
from os import listdir, scandir


def print_category_hours(
    cat_hours: dict[str, float], sort_by: str = "name", descending: bool = False
) -> List[str]:
    """Prints out the category hour dict."""
    cat: str
    loop_order: List[str]
    if sort_by == "name":
        loop_order = sorted(
            cat_hours.keys(), key=lambda x: x.lower(), reverse=descending
        )
    elif sort_by == "hours":
        loop_order = sorted(cat_hours, key=lambda x: cat_hours[x], reverse=descending)
    else:
        raise Exception("Invalid Sort_by method provided.")

    print_lines: List[str] = []
    for cat in loop_order:
        hours: float = cat_hours[cat]
        print_str: str = f"{cat}: {hours}"
        print(print_str)
        print_lines.append(print_str)
    return print_lines


@click.group()
def main() -> None:
    pass


@main.command()
@click.argument("file_src")
def parse_file(file_src: str) -> None:
    """Parses a markdown file."""
    lines = load_and_parse_file(file_src)
    timeblock_list = create_timeblocks_list(lines)
    cat_hours: dict[str, float] = calc_category_hours(timeblock_list)
    print_category_hours(cat_hours)


@main.command()
@click.argument("scan_dir")
@click.option(
    "-m",
    default="(\d{4}-\d{2}-\d{2}).md",
    help="Files to match. Defaults to '(\d{4}-\d{2}-\d{2}).md'.",
)
@click.option(
    "--date-strp",
    default="%Y-%m-%d",
    help="Match string to provide strptime function to grab datetime from filename date. Requires date to be grouped in -m string",
)
@click.option(
    "--sort-by",
    default="name",
    help="What to sort by, 'name' or 'hours'. Defaults 'name'.",
)
@click.option(
    "--descending",
    default=False,
    help="Print results in decending order instead of ascending. Defaults to False.",
)
def parse_dir(
    scan_dir: str,
    m: str = ".md",
    date_strp: str = "%Y-%m-%d",
    sort_by: str = "name",
    descending: bool = False,
) -> None:
    """Parses a directory of markdown files."""
    timeblock_list = dir_timeblocks(scan_dir, file_match=m, strp_str=date_strp)
    cat_hours: dict[str, float] = calc_category_hours(timeblock_list)
    print_category_hours(cat_hours, sort_by=sort_by, descending=descending)


if __name__ == "__main__":
    main()

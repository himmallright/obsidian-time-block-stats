from otbs.parser import create_timeblocks_list, load_and_parse_file, dir_timeblocks
from otbs.stats import calc_category_hours


def test_calc_category_hours(test_day_file):
    # Might want to replace test set with one to feed directly to the function eventually...
    file_data = load_and_parse_file(test_day_file)
    timeblock_list = create_timeblocks_list(file_data)
    cat_times = calc_category_hours(timeblock_list)
    assert cat_times["cleaning"] == 60
    assert cat_times["website"] == 285
    assert cat_times["workout"] == 35
    assert cat_times["eating"] == 40
    assert cat_times["idk"] == 10


def test_dir_timeblocks_category_hours(category_hours):
    assert category_hours["cleaning"] == 120
    assert category_hours["website"] == 570
    assert category_hours["workout"] == 70
    assert category_hours["eating"] == 80
    assert category_hours["idk"] == 20

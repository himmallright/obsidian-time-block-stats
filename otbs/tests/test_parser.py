from otbs.parser import *

import pytest


def test_planner_parse(test_day_file):
    """Tests loading the test file. Shallow tests, just checks it grabbed correct number of lines."""
    data = load_and_parse_file(test_day_file)
    assert len(data) == 6


def test_dir_time_blocks_filter(test_day_dir):
    """Tests loading all files from a dir."""
    data = dir_timeblocks(test_day_dir, file_match="(\d{4}-\d{2}-\d{2})_copy.md")
    assert len(data) == 6
    data2 = dir_timeblocks(test_day_dir, file_match="(\d{4}-\d{2}-\d{2})")
    assert len(data2) == 12


def test_datetime_filename_date_set(test_day_dir):
    """Tests that the time blocks correctly pull and set the date from the test filenames."""
    data = dir_timeblocks(test_day_dir, file_match="(\d{4}-\d{2}-\d{2})")
    for block in data:
        assert block.time_range[0].day == 2
        assert block.time_range[0].month == 2
        assert block.time_range[0].year == 2020


def test_create_timeblock_list(timeblock_test_data):
    block1_data, block2_data = timeblock_test_data
    timeblock_list = create_timeblocks_list(timeblock_test_data)
    block1, block2 = timeblock_list
    assert block1.name == block1_data[1]
    assert block2.name == block2_data[1]
    assert block1.category == block1_data[2]
    assert block2.category == block2_data[2]
    assert block1.time_range[0].hour == 8
    assert block1.time_range[0].minute == 10
    assert block1.time_range[1].hour == 8
    assert block1.time_range[1].minute == 30
    assert block2.time_range[0].hour == 8
    assert block2.time_range[0].minute == 30
    assert block2.time_range[1].hour == 8
    assert block2.time_range[1].minute == 40
    assert block1.duration == 20
    assert block2.duration == 10


@pytest.mark.parametrize(
    "time_str, expected_pair",
    [("19:14", (19, 14)), ("04:12", (4, 12)), ("4:53", (4, 53))],
    ids=lambda x: x[0],
)
def test_convert_time(time_str, expected_pair):
    """Tests the simple function to convert the time strings."""
    date = convert_time(time_str)
    assert date.hour == expected_pair[0]
    assert date.minute == expected_pair[1]


@pytest.mark.parametrize(
    "test_pair", [("#today", "today"), ("#next-week", "next-week")], ids=lambda x: x[0]
)
def test_format_category(test_pair):
    """Test the fn to strip and format the category strings."""
    assert format_category(test_pair[0]) == test_pair[1]

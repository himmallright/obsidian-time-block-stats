"""Test the print functions called for the CLI output."""
from otbs.cli import print_category_hours

import pytest

from otbs.cli import print_category_hours

print_category_hours_test_values = [
    # sort by arg, descending arg, expected output
    (
        "name",
        False,
        [
            "cleaning: 120.0",
            "eating: 80.0",
            "idk: 20.0",
            "website: 570.0",
            "workout: 70.0",
        ],
    ),
    (
        "name",
        True,
        [
            "workout: 70.0",
            "website: 570.0",
            "idk: 20.0",
            "eating: 80.0",
            "cleaning: 120.0",
        ],
    ),
    (
        "hours",
        False,
        [
            "idk: 20.0",
            "workout: 70.0",
            "eating: 80.0",
            "cleaning: 120.0",
            "website: 570.0",
        ],
    ),
    (
        "hours",
        True,
        [
            "website: 570.0",
            "cleaning: 120.0",
            "eating: 80.0",
            "workout: 70.0",
            "idk: 20.0",
        ],
    ),
]


def cat_hours_ids(param_data):
    id_list = []
    for data in param_data:
        id_list.append(f"{data[0]}-{str(data[1])}")
    return id_list


@pytest.mark.parametrize(
    "sort_by, descending, expected_output",
    print_category_hours_test_values,
    ids=cat_hours_ids(print_category_hours_test_values),
)
def test_print_category_hours(category_hours, sort_by, descending, expected_output):
    print_lines = print_category_hours(
        category_hours, sort_by=sort_by, descending=descending
    )
    for id, line in enumerate(print_lines):
        assert line.strip() == expected_output[id]

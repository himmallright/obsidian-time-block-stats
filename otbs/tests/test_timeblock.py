from otbs.parser import convert_time
from otbs.timeblock import *
import pytest


@pytest.mark.parametrize(
    "name,cat,start,end,expected_duration",
    [
        ("name", "general", "09:30", "10:31", 61),
        ("name", "general", "10:43", "14:17", 214),
    ],
)
def test_timeblock_obj(name, cat, start, end, expected_duration):
    """Test creating and setting timeblock objects."""
    block = Timeblock(name, cat, convert_time(start), convert_time(end))
    assert block.name == name
    assert block.category == cat
    # assert block.time_range == (start, end)
    assert block.duration == expected_duration

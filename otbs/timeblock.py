"""A base object that contains information about each parsed time-block."""
from typing import Tuple
from datetime import datetime, timedelta


class Timeblock:
    def __init__(
        self, task_name: str, category: str, start_time: datetime, end_time: datetime
    ) -> None:
        self.name: str = task_name
        self.category: str = category
        self.time_range: Tuple[datetime, datetime] = (start_time, end_time)
        self.duration: float = self.calc_duration(start_time, end_time)

    def __repr__(self) -> str:
        return f"{self.name} ({self.time_range[0].strftime('%H:%M')}-{self.time_range[1].strftime('%H:%M')})"

    def calc_duration(self, start_time: datetime, end_time: datetime) -> float:
        """Calculated the duration in minutes for a timeblock"""
        diff: timedelta = end_time - start_time
        return diff.seconds / 60.0

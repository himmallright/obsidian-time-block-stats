from otbs.parser import dir_timeblocks
from otbs.stats import calc_category_hours
from pkg_resources import resource_filename
import pytest


@pytest.fixture()
def test_day_file():
    return resource_filename("otbs", "tests/data/2020-02-02.md")


@pytest.fixture()
def test_day_dir():
    return resource_filename("otbs", "tests/data/")


@pytest.fixture()
def timeblock_test_data():
    return [("08:10", "Coffee", "planning"), ("08:30", "Standup", "meeting")]


@pytest.fixture()
def category_hours(test_day_dir):
    timeblock_list = dir_timeblocks(test_day_dir, file_match="md", strp_str=None)
    return calc_category_hours(timeblock_list)

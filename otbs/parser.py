from datetime import datetime, timedelta
from typing import List, Tuple, Optional
from os import listdir
from os.path import basename

from otbs.timeblock import Timeblock
import re


def load_and_parse_file(file_src: str) -> List[Tuple[str, str, str]]:
    """Loads and parses planner file."""
    lines: List[Tuple[str, str, str]] = []
    r = re.compile("\s*- \[.+\]\s+(\d+:\d+)\s+(.*)(\#.*)")
    with open(file_src) as f:
        for line in f:
            match: List[Tuple[str, str, str]] = r.findall(line)
            if match:
                time_str, task_name, category = match[0]
                lines.append((time_str, task_name, format_category(category)))
    lines.sort(key=lambda y: y[0])
    return lines


def file_timeblocks(file_src: str, last_block_duration: int = 10) -> list[Timeblock]:
    """Parses a file and returns a list of timeblock objects for it."""
    filename: str = basename(file_src)
    file_data: List[Tuple[str, str, str]] = load_and_parse_file(file_src)
    timeblock_list: list[Timeblock] = create_timeblocks_list(
        file_data, last_block_duration=last_block_duration
    )
    return timeblock_list


def dir_timeblocks(
    dir_src: str,
    file_match: str = "(\d{4}-\d{2}-\d{2}).md",
    strp_str: Optional[str] = "%Y-%m-%d",
    last_block_duration: int = 10,
) -> List[Timeblock]:
    """Loads and parses all planner files in a dir."""
    r = re.compile(file_match)
    dir_files: List[str] = listdir(dir_src)
    timeblock_list: list[Timeblock] = []
    for f in dir_files:
        matches: List[str] = r.findall(f)
        if matches:
            file_data: List[Tuple[str, str, str]] = load_and_parse_file(dir_src + f)
            start_datetime: Optional[datetime]
            if strp_str:
                start_datetime = datetime.strptime(matches[0], strp_str)
            else:
                start_datetime = None
            timeblock_list += create_timeblocks_list(
                file_data,
                last_block_duration=last_block_duration,
                init_datetime=start_datetime,
            )
    return timeblock_list


def create_timeblocks_list(
    line_list: List[Tuple[str, str, str]],
    init_datetime: Optional[datetime] = None,
    last_block_duration: int = 10,
) -> list[Timeblock]:
    """Creates the list of timblock objects for a parsed file."""
    timeblock_list = []
    for id, block_data in enumerate(line_list):
        start_time: str
        end_time: datetime
        name: str
        category: str
        start_time, name, category = block_data
        if id == len(line_list) - 1:
            # Last Item
            end_time = convert_time(
                start_time, init_datetime=init_datetime
            ) + timedelta(minutes=last_block_duration)
            timeblock_list.append(
                Timeblock(
                    name,
                    category,
                    convert_time(start_time, init_datetime=init_datetime),
                    end_time,
                )
            )
        else:
            end_time = convert_time(line_list[id + 1][0], init_datetime=init_datetime)
            timeblock_list.append(
                Timeblock(
                    name,
                    category,
                    convert_time(start_time, init_datetime=init_datetime),
                    end_time,
                )
            )
    return timeblock_list


def convert_time(time_str: str, init_datetime: Optional[datetime] = None) -> datetime:
    """Turns the time string in a block set to a datetime time."""
    t: List[str] = time_str.split(":")
    hour: str
    minute: str
    hour, minute = t
    if init_datetime:
        date = init_datetime
    else:
        date = datetime.now()
    return date.replace(hour=int(hour), minute=int(minute), second=0, microsecond=0)


def format_category(cat_string: str) -> str:
    """Parses and formats the category string"""
    # For now, just strip the #
    return cat_string.replace("#", "")

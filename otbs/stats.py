from typing import List
from otbs.timeblock import Timeblock


def calc_category_hours(timeblock_list: List[Timeblock]) -> dict[str, float]:
    """Calculates to total time per category of a timeblock list."""
    category_hours: dict[str, float] = {}
    for timeblock in timeblock_list:
        if timeblock.category in category_hours.keys():
            category_hours[timeblock.category] += timeblock.duration
        else:
            category_hours[timeblock.category] = timeblock.duration
    return category_hours

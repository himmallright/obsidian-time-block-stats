![Pipeline Status](https://gitlab.com/himmallright/obsidian-time-block-stats/badges/main/pipeline.svg?ignore_skipped=true)

# obsidian-time-block-stats

Small tool to calculate and run some stats on my daily planner time blocks in obsidian

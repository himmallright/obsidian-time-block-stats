check:
	black --check --diff .
lint:
	pre-commit run --all-files
dev-install:
	pip install --editable .
test:
	pytest -v .
